package server.brestovac;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.jetty.websocket.WebSocket;
import org.eclipse.jetty.websocket.WebSocketHandler;

import sql.brestovac.SQLiteJDBC;

public class ChatWebSocketHandler extends WebSocketHandler {

	private final Set<ChatWebSocket> webSockets = new CopyOnWriteArraySet<ChatWebSocket>();
	Set<String> users;
	boolean value;
	List<String> list_of_strings = new ArrayList<String>();
	Timer timer = new Timer();
	SQLiteJDBC base = new SQLiteJDBC();
	private String[] arrayOdAndroid;
	String result = "";
	String resultStari = "";

	public WebSocket doWebSocketConnect(HttpServletRequest request,
			String protocol) {
		return new ChatWebSocket();
	}

	private class ChatWebSocket implements WebSocket.OnTextMessage {

		private Connection connection;

		public void onOpen(Connection connection) {
			// Client (Browser) WebSockets has opened a connection.
			// 1) Store the opened connection
			this.connection = connection;
			// 2) Add ChatWebSocket in the global list of ChatWebSocket
			// instances
			// instance.
			// ((WebSocketHandler) this.connection).setMaxIdleTime(1440000);
			webSockets.add(this);
			System.out.println("Connected client: "
					+ this.connection.toString());

		}

		public void onMessage(String data) {

			arrayOdAndroid = data.split("#");

			if (arrayOdAndroid[0].equals("SELECT_LISTA")) {

				ArrayList<String> a = new ArrayList<String>();
				if (arrayOdAndroid[2].equals("SCHEDULED")) {
					a = base.getScheduledGames(arrayOdAndroid[1]);

				} else if (arrayOdAndroid[2].equals("FINISHED")) {
					a = base.getFinishedGames();

				} else if (arrayOdAndroid[2].equals("LIVE")) {
					a.clear();
					a = base.getLiveGames();

				} else if (arrayOdAndroid[2].equals("MY")) {
					a.clear();
					a = base.getMyGames(arrayOdAndroid[1]);

				}

				for (String s : a) {
					result = result.concat(s);
				}

				result = arrayOdAndroid[1] + "#" + "Lista" + "#"
						+ arrayOdAndroid[2] + "#" + result;

				for (ChatWebSocket webSocket : webSockets) {
					try {
						webSocket.connection.sendMessage(result);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				result = new String("");

			}

			if (arrayOdAndroid[0].equals("TOKEN_NUM")) {

				String broj_tokena = null;
				String resultBrojTokena;
				broj_tokena = base.getTokenNumber(arrayOdAndroid[1]);

				resultBrojTokena = arrayOdAndroid[1] + "#" + "Tokens" + "#"
						+ broj_tokena;

				for (ChatWebSocket webSocket : webSockets) {
					try {
						webSocket.connection.sendMessage(resultBrojTokena);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				resultBrojTokena = new String("");

			}

			if (arrayOdAndroid[0].equals("ADD_TIP")) {

				base.insertGameUser(arrayOdAndroid[1], arrayOdAndroid[2]);

			}

			if (arrayOdAndroid[0].equals("PAYMENT")) {

				base.updateUplata(arrayOdAndroid[1], arrayOdAndroid[2],
						arrayOdAndroid[3], arrayOdAndroid[4]);

			}

		}

		public void onClose(int closeCode, String message) {
			// Remove ChatWebSocket in the global list of ChatWebSocket
			// instance.
			for (ChatWebSocket webSocket : webSockets) {
				webSockets.remove(this);
				webSocket.connection.disconnect();
			}
		}
	}
}
