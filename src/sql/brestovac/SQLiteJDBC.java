package sql.brestovac;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class SQLiteJDBC {

	public static Connection c = null;
	public static Statement stmt = null;
	public static boolean empty;

	// ESTABLISH CONNECTION TO DATABASE AND CREATING TABLES
	public void initDB() {

		try {
			String homePath = System.getProperty("user.home");
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + homePath
					+ "\\dbinfobipDatabaseW2.db");
			c.setAutoCommit(false);
			System.out.println("Opened database successfully");

			stmt = c.createStatement();

			// CREATE a Tables
			String sqlTablicaGame = " CREATE TABLE IF NOT EXISTS GAME "
					+ " (ID_Game INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,  "
					+ " Home_team VARCHAR(52) NOT NULL, "
					+ " Away_team VARCHAR(52) NOT NULL, "
					+ " Vrijeme BIGINT NOT NULL, "
					+ " Rezultat_home_team INTEGER DEFAULT 0, "
					+ " Rezultat_away_team INTEGER DEFAULT 0, "
					+ " Finished INT NOT NULL, " + " TIP CHAR NOT NULL )";
			stmt.executeUpdate(sqlTablicaGame);

			String sqlTablicaGameUser = "CREATE TABLE IF NOT EXISTS GAME_USER "
					+ " (ID_Game INTEGER NOT NULL,  "
					+ " Broj_telefona_korisnika VARCHAR(52) NOT NULL, "
					+ " FOREIGN KEY(ID_Game) REFERENCES GAME(ID_Game), "
					+ " FOREIGN KEY(Broj_telefona_korisnika) REFERENCES USER(Broj_telefona_korisnika)) ";
			stmt.executeUpdate(sqlTablicaGameUser);

			String sqlTablicaUplata = "CREATE TABLE IF NOT EXISTS UPLATA "
					+ " (ID_Uplata VARCHAR PRIMARY KEY  NOT NULL, "
					+ " Iznos_uplata INTEGER DEFAULT 0, "
					+ " Broj_telefona_korisnika VARCHAR(52) NOT NULL, "
					+ " FOREIGN KEY(Broj_telefona_korisnika) REFERENCES USER(Broj_telefona_korisnika)) ";
			stmt.executeUpdate(sqlTablicaUplata);

			String sqlTablicaUser = "CREATE TABLE IF NOT EXISTS USER "
					+ " (Broj_telefona_korisnika VARCHAR(52) NOT NULL,  "
					+ " Broj_tokena INTEGER DEFAULT 0 ) ";
			stmt.executeUpdate(sqlTablicaUser);

			c.commit();
			stmt.clearBatch();
			stmt.close();
			c.close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

	}

	public ArrayList<String> getMyGames(String phoneNumber) {
		ArrayList<String> myGames = new ArrayList<String>();

		try {
			String homePath = System.getProperty("user.home");
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + homePath
					+ "\\dbinfobipDatabaseW2.db");
			c.setAutoCommit(false);
			System.out.println("Opened database successfully");

			stmt = c.createStatement();

			String sql = "SELECT * FROM GAME_USER WHERE Broj_telefona_korisnika = '"
					+ phoneNumber + "';";
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int gameId = rs.getInt("ID_Game");
				String game = getGameById(gameId);
				myGames.add(game);
			}
			c.commit();
			stmt.clearBatch();
			stmt.close();
			c.close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

		return myGames;
	}

	String getGameById(int gameId) {
		String game = null;
		try {

			stmt = c.createStatement();

			String sql = "SELECT * FROM GAME WHERE ID_Game = '" + gameId + "';";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				game = rs.getInt("ID_Game") + "," + rs.getString("Home_team")
						+ "," + rs.getString("Away_team") + ","
						+ rs.getLong("Vrijeme") + ","
						+ rs.getInt("Rezultat_home_team") + ","
						+ rs.getInt("Rezultat_away_team") + ","
						+ rs.getInt("Finished") + "," + rs.getString("TIP");
				game = game + "#";
			}
			c.commit();
			stmt.clearBatch();
			stmt.close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		return game;
	}

	public ArrayList<String> getFinishedGames() {
		ArrayList<String> finishedGames = new ArrayList<String>();
		String game = null;
		try {
			String homePath = System.getProperty("user.home");
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + homePath
					+ "\\dbinfobipDatabaseW2.db");
			c.setAutoCommit(false);
			System.out.println("Opened database successfully");

			stmt = c.createStatement();

			String sql = "SELECT * FROM GAME WHERE Finished > 0;";
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				game = rs.getInt("ID_Game") + "," + rs.getString("Home_team")
						+ "," + rs.getString("Away_team") + ","
						+ rs.getLong("Vrijeme") + ","
						+ rs.getInt("Rezultat_home_team") + ","
						+ rs.getInt("Rezultat_away_team") + ","
						+ rs.getInt("Finished") + "," + rs.getString("TIP");
				game = game + "#";
				finishedGames.add(game);
			}
			c.commit();
			stmt.clearBatch();
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

		return finishedGames;
	}

	public ArrayList<String> getLiveGames() {
		ArrayList<String> liveGames = new ArrayList<String>();
		long time = System.currentTimeMillis();
		String game = null;
		try {
			String homePath = System.getProperty("user.home");
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + homePath
					+ "\\dbinfobipDatabaseW2.db");
			c.setAutoCommit(false);
			System.out.println("Opened database successfully");

			stmt = c.createStatement();

			String sql = "SELECT * FROM GAME WHERE Finished = 0 AND Vrijeme < "
					+ time + ";";
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				game = rs.getInt("ID_Game") + "," + rs.getString("Home_team")
						+ "," + rs.getString("Away_team") + ","
						+ rs.getLong("Vrijeme") + ","
						+ rs.getInt("Rezultat_home_team") + ","
						+ rs.getInt("Rezultat_away_team") + ","
						+ rs.getInt("Finished") + "," + rs.getString("TIP");
				game = game + "#";
				liveGames.add(game);
			}
			c.commit();
			stmt.clearBatch();
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

		return liveGames;
	}

	public ArrayList<String> getScheduledGames(String phoneNumber) {
		ArrayList<String> scheduledGames = new ArrayList<String>();
		ArrayList<String> myGamesIds = getMyGamesIds(phoneNumber);
		long time = System.currentTimeMillis();
		String game;

		try {
			String homePath = System.getProperty("user.home");
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + homePath
					+ "\\dbinfobipDatabaseW2.db");
			c.setAutoCommit(false);
			System.out.println("Opened database successfully");

			stmt = c.createStatement();

			String sql = "SELECT * FROM GAME WHERE Vrijeme > " + time + ";";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				int gameId = rs.getInt("ID_Game");
				if (myGamesIds.contains(gameId + ""))
					continue;
				game = rs.getInt("ID_Game") + "," + rs.getString("Home_team")
						+ "," + rs.getString("Away_team") + ","
						+ rs.getLong("Vrijeme") + ","
						+ rs.getInt("Rezultat_home_team") + ","
						+ rs.getInt("Rezultat_away_team") + ","
						+ rs.getInt("Finished") + "," + rs.getString("TIP");
				game = game + "#";
				scheduledGames.add(game);
			}
			c.commit();
			stmt.clearBatch();
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

		return scheduledGames;
	}

	ArrayList<String> getMyGamesIds(String phoneNumber) {
		ArrayList<String> myGamesIds = new ArrayList<String>();

		try {
			String homePath = System.getProperty("user.home");
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + homePath
					+ "\\dbinfobipDatabaseW2.db");
			c.setAutoCommit(false);
			System.out.println("Opened database successfully");

			Statement stmt = c.createStatement();

			String sql = "SELECT * FROM GAME_USER WHERE Broj_telefona_korisnika = '"
					+ phoneNumber + "';";
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int gameId = rs.getInt("ID_Game");
				myGamesIds.add(String.valueOf(gameId));
			}
			c.commit();
			stmt.clearBatch();
			stmt.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

		return myGamesIds;
	}

	public String getTokenNumber(String brojTelefona) {
		String broj_tokena = null;
		try {
			String homePath = System.getProperty("user.home");
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + homePath
					+ "\\dbinfobipDatabaseW2.db");
			c.setAutoCommit(false);
			System.out.println("Opened database successfully");

			stmt = c.createStatement();
			empty = true;
			String sql = "SELECT * FROM USER WHERE Broj_telefona_korisnika = '"
					+ brojTelefona + "';";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				empty = false;
				broj_tokena = String.valueOf(rs.getInt("Broj_tokena"));

				broj_tokena = broj_tokena + "#";
				System.out.println("---------broj tokena je" + broj_tokena);
			}
			c.commit();
			stmt.clearBatch();
			stmt.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

		if (empty) {
			broj_tokena = new String("0");
		}
		return broj_tokena;
	}

	public void insertGameUser(String brojTelefona, String ID_Game) {
		int broj_tokena = 0;
		try {
			String homePath = System.getProperty("user.home");
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + homePath
					+ "\\dbinfobipDatabaseW2.db");
			c.setAutoCommit(false);
			System.out.println("Opened database successfully");

			stmt = c.createStatement();

			String sqlInsertTablicaGameUser = "INSERT INTO GAME_USER (Broj_telefona_korisnika, ID_Game) "
					+ "VALUES ( '" + brojTelefona + "','" + ID_Game + "');";
			stmt.executeUpdate(sqlInsertTablicaGameUser);
			stmt.clearBatch();
			stmt.close();

			empty = true;
			String sql = "SELECT Broj_tokena FROM USER WHERE Broj_telefona_korisnika = '"
					+ brojTelefona + "';";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				broj_tokena = rs.getInt("Broj_tokena");
				empty = false;
			}

			int novi_Broj_Tokena = broj_tokena - 5;

			if (empty) {

				stmt = c.createStatement();
				String sqlInsertTablicaUplataIzAndroida = "INSERT INTO USER (Broj_telefona_korisnika, Broj_tokena) "
						+ "VALUES ( '" + brojTelefona + "', '0' );";
				stmt.executeUpdate(sqlInsertTablicaUplataIzAndroida);

			} else {

				stmt = c.createStatement();
				String sqlUpdateTablicaSifri = "UPDATE USER set Broj_tokena = '"
						+ novi_Broj_Tokena
						+ "' WHERE Broj_telefona_korisnika='"
						+ brojTelefona
						+ "';";
				stmt.executeUpdate(sqlUpdateTablicaSifri);

			}

			c.commit();
			stmt.clearBatch();
			stmt.close();
			c.close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

	}

	public void updateUplata(String brojTelefona, String brojTokena,
			String iznosUplata, String ID_Uplata) {

		int broj_tokena = 0;
		try {
			String homePath = System.getProperty("user.home");
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + homePath
					+ "\\dbinfobipDatabaseW2.db");
			c.setAutoCommit(false);
			System.out.println("Opened database successfully");

			stmt = c.createStatement();

			empty = true;
			ResultSet rsIdUplata = stmt
					.executeQuery("SELECT * FROM UPLATA WHERE ID_Uplata='"
							+ ID_Uplata + "';");
			while (rsIdUplata.next()) {
				empty = false;
			}
			if (empty) {

				String sqlInsertTablicaUplataIzAndroida = "INSERT INTO UPLATA (ID_Uplata, Iznos_uplata, Broj_telefona_korisnika) "
						+ "VALUES ( '"
						+ ID_Uplata
						+ "','"
						+ iznosUplata
						+ "','" + brojTelefona + "' );";
				stmt.executeUpdate(sqlInsertTablicaUplataIzAndroida);

			} else {

				String sqlUpdateTablicaSifri = "UPDATE UPLATA set Iznos_uplata = '"
						+ iznosUplata
						+ "' AND ID_Uplata='"
						+ ID_Uplata
						+ "' WHERE Broj_telefona_korisnika='"
						+ brojTelefona
						+ "';";
				stmt.executeUpdate(sqlUpdateTablicaSifri);

			}
			stmt.clearBatch();
			stmt.close();
			c.commit();

			stmt = c.createStatement();

			empty = true;
			ResultSet rsBrojTelefonaKorisnika = stmt
					.executeQuery("SELECT * FROM USER WHERE Broj_telefona_korisnika='"
							+ brojTelefona + "';");
			while (rsBrojTelefonaKorisnika.next()) {
				empty = false;
				broj_tokena = rsBrojTelefonaKorisnika.getInt("Broj_tokena");
			}

			int novi_Broj_Tokena = broj_tokena + Integer.parseInt(brojTokena);

			if (empty) {
				String sqlInsertTablicaUplataIzAndroida = "INSERT INTO USER (Broj_telefona_korisnika, Broj_tokena) "
						+ "VALUES ( '"
						+ brojTelefona
						+ "','"
						+ brojTokena
						+ "' );";
				stmt.executeUpdate(sqlInsertTablicaUplataIzAndroida);
			} else {
				String sqlUpdateTablicaSifri = "UPDATE USER set Broj_tokena = '"
						+ novi_Broj_Tokena
						+ "' WHERE Broj_telefona_korisnika='"
						+ brojTelefona
						+ "';";
				stmt.executeUpdate(sqlUpdateTablicaSifri);
			}

			c.commit();
			stmt.clearBatch();
			stmt.close();
			c.close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}

}
