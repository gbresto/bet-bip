package sql.brestovac.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

public class SQLiteJDBCTest {

	public static Connection c = null;
	public static Statement stmt = null;
	public static boolean empty;

	// ESTABLISH CONNECTION TO DATABASE AND CREATING TABLES
	public void initDB() {

		try {
			String homePath = System.getProperty("user.home");
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + homePath
					+ "\\dbinfobipDatabaseW2.db");
			c.setAutoCommit(false);
			System.out.println("Opened database successfully");

			stmt = c.createStatement();

			// CREATE a Tables
			String sqlTablicaGame = " CREATE TABLE IF NOT EXISTS GAME "
					+ " (ID_Game INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,  "
					+ " Home_team VARCHAR(52) NOT NULL, "
					+ " Away_team VARCHAR(52) NOT NULL, "
					+ " Vrijeme BIGINT NOT NULL, "
					+ " Rezultat_home_team INTEGER DEFAULT 0, "
					+ " Rezultat_away_team INTEGER DEFAULT 0, "
					+ " Finished INT NOT NULL, " + " TIP CHAR NOT NULL )";
			stmt.executeUpdate(sqlTablicaGame);

			String sqlTablicaGameUser = "CREATE TABLE IF NOT EXISTS GAME_USER "
					+ " (ID_Game INTEGER NOT NULL,  "
					+ " Broj_telefona_korisnika VARCHAR(52) NOT NULL, "
					+ " FOREIGN KEY(ID_Game) REFERENCES GAME(ID_Game), "
					+ " FOREIGN KEY(Broj_telefona_korisnika) REFERENCES USER(Broj_telefona_korisnika)) ";
			stmt.executeUpdate(sqlTablicaGameUser);

			String sqlTablicaUplata = "CREATE TABLE IF NOT EXISTS UPLATA "
					+ " (ID_Uplata VARCHAR PRIMARY KEY  NOT NULL, "
					+ " Iznos_uplata INTEGER DEFAULT 0, "
					+ " Broj_telefona_korisnika VARCHAR(52) NOT NULL, "
					+ " FOREIGN KEY(Broj_telefona_korisnika) REFERENCES USER(Broj_telefona_korisnika)) ";
			stmt.executeUpdate(sqlTablicaUplata);

			String sqlTablicaUser = "CREATE TABLE IF NOT EXISTS USER "
					+ " (Broj_telefona_korisnika VARCHAR(52) NOT NULL,  "
					+ " Broj_tokena INTEGER DEFAULT 0 ) ";
			stmt.executeUpdate(sqlTablicaUser);

			c.commit();
			stmt.clearBatch();
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

	}

	// INSERT SOME PREDEFINED VALUES INTO DATABASE
	public void insertIntoDB() {

		try {
			String homePath = System.getProperty("user.home");
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + homePath
					+ "\\dbinfobipDatabaseW2.db");
			c.setAutoCommit(false);
			System.out.println("Opened database successfully");

			stmt = c.createStatement();

			// true = 1, false = 0
			String sqlInsertTablicaGame = "INSERT INTO GAME (Home_team, Away_team, Vrijeme, Rezultat_home_team, Rezultat_away_team, Finished, TIP) "
					+ "VALUES ( 'Barcelona', 'Real Madrid', '1390486262138', '4', '3', '1', '2' );"; // pocela
																										// i
																										// zavrsila
			stmt.executeUpdate(sqlInsertTablicaGame);

			String sqlInsertTablicaGame2 = "INSERT INTO GAME (Home_team, Away_team, Vrijeme, Rezultat_home_team, Rezultat_away_team, Finished, TIP) "
					+ "VALUES ( 'Rijeka', 'Dinamo', '1390486263138', '2', '0', '1', '2' );"; // pocela
																								// i
																								// zavrsila
			stmt.executeUpdate(sqlInsertTablicaGame2);

			String sqlInsertTablicaGame3 = "INSERT INTO GAME (Home_team, Away_team, Vrijeme, Rezultat_home_team, Rezultat_away_team, Finished, TIP) "
					+ "VALUES ( 'Rijeka', 'Partizan', '1390513317097', '2', '0', '0', '2' );";
			stmt.executeUpdate(sqlInsertTablicaGame3); // nije pocela, nije
														// zavrsila

			String sqlInsertTablicaGame4 = "INSERT INTO GAME (Home_team, Away_team, Vrijeme, Rezultat_home_team, Rezultat_away_team, Finished, TIP) "
					+ "VALUES ( 'Zvezda', 'Partizan', '1390486262138', '2', '0', '0', '2' );";
			stmt.executeUpdate(sqlInsertTablicaGame4); // pocelo, nije zavrsilo

			String sqlInsertTablicaGameUser = "INSERT INTO GAME_USER (Broj_telefona_korisnika, ID_Game) "
					+ "VALUES ( '+385911504217', '1' );";
			stmt.executeUpdate(sqlInsertTablicaGameUser);

			String sqlInsertTablicaGameUser2 = "INSERT INTO GAME_USER (Broj_telefona_korisnika, ID_Game) "
					+ "VALUES ( '+385911504217', '2' );";
			stmt.executeUpdate(sqlInsertTablicaGameUser2);

			String sqlInsertTablicaGameUser3 = "INSERT INTO GAME_USER (Broj_telefona_korisnika, ID_Game) "
					+ "VALUES ( '+385996859990', '3' );";
			stmt.executeUpdate(sqlInsertTablicaGameUser3);

			String sqlInsertTablicaGameUser4 = "INSERT INTO GAME_USER (Broj_telefona_korisnika, ID_Game) "
					+ "VALUES ( '+385996859990', '4' );";
			stmt.executeUpdate(sqlInsertTablicaGameUser4);

			String sqlInsertTablicaUplata = "INSERT INTO UPLATA (ID_Uplata, Iznos_uplata, Broj_telefona_korisnika) "
					+ "VALUES ( '1', '40', '+385911504217' );";
			stmt.executeUpdate(sqlInsertTablicaUplata);

			String sqlInsertTablicaUplata2 = "INSERT INTO UPLATA (ID_Uplata, Iznos_uplata, Broj_telefona_korisnika) "
					+ "VALUES ( '2', '80', '+385996859990' );";
			stmt.executeUpdate(sqlInsertTablicaUplata2);

			String sqlInsertTablicaUser = "INSERT INTO USER (Broj_telefona_korisnika, Broj_tokena) "
					+ "VALUES ('+385911504217',  '90' );";
			stmt.executeUpdate(sqlInsertTablicaUser);

			String sqlInsertTablicaUser2 = "INSERT INTO USER (Broj_telefona_korisnika, Broj_tokena) "
					+ "VALUES ('+385996859990',  '80' );";
			stmt.executeUpdate(sqlInsertTablicaUser2);

			c.commit();
			stmt.clearBatch();
			stmt.close();
			c.close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}

	// TEST FOR INSERTING VALUES TO DB FROM ANDROID
	public void insertToDBFromAndroid(String Home_team, String Away_team,
			int Rezultat_home_team, int Rezultat_away_team, int finished,
			int tip) {
		Date n = new Date();

		try {
			String homePath = System.getProperty("user.home");
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + homePath
					+ "\\dbinfobipDatabaseW2.db");
			c.setAutoCommit(false);
			System.out.println("Opened database successfully");

			stmt = c.createStatement();

			String sqlInsertTablicaGame = "INSERT INTO GAME (Home_team, Away_team, Vrijeme, Rezultat_home_team, Rezultat_away_team, Finished, TIP) "
					+ "VALUES ( '"
					+ Home_team
					+ "', '"
					+ Away_team
					+ "', '"
					+ n.getTime()
					+ "', '"
					+ Rezultat_home_team
					+ "', '"
					+ Rezultat_away_team
					+ "', '"
					+ finished
					+ "', '"
					+ tip
					+ "' );";
			stmt.executeUpdate(sqlInsertTablicaGame);

			c.commit();
			stmt.clearBatch();
			stmt.close();
			c.close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}

	// TEST FOR SELECTING VALUES FROM DB AND SENDING THEM TO ANDROID
	public ArrayList<String> selectFromDBtoAndroid(String Home_team) {

		ArrayList<String> keyList = new ArrayList<String>();

		try {
			String homePath = System.getProperty("user.home");
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + homePath
					+ "\\dbinfobipDatabaseW2.db");
			c.setAutoCommit(false);
			System.out.println("Opened database successfully");

			stmt = c.createStatement();

			// Get data for home team
			ResultSet rsGetHomeTeam = stmt
					.executeQuery("SELECT * FROM GAME WHERE Home_team='"
							+ Home_team + "';");
			while (rsGetHomeTeam.next()) {
				keyList.add(rsGetHomeTeam.getString("Home_team"));

				keyList.add(rsGetHomeTeam.getString("Away_team"));

				long vrijeme = rsGetHomeTeam.getLong("Vrijeme");
				keyList.add(String.valueOf(vrijeme));

				int Rezultat_home_team = rsGetHomeTeam
						.getInt("Rezultat_home_team");
				keyList.add(String.valueOf(Rezultat_home_team));

				int Rezultat_away_team = rsGetHomeTeam
						.getInt("Rezultat_away_team");
				keyList.add(String.valueOf(Rezultat_away_team));

				int Finished = rsGetHomeTeam.getInt("Finished");
				keyList.add(String.valueOf(Finished));

				keyList.add(rsGetHomeTeam.getString("TIP"));
			}

			c.commit();
			stmt.clearBatch();
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

		return keyList;
	}

	/*
	 * public static void main( String args[] ) {
	 * 
	 * try {
	 * 
	 * SQLiteJDBC n = new SQLiteJDBC(); //n.initDB(); // n.insertIntoDB();
	 * 
	 * 
	 * } catch ( Exception e ) { System.err.println( e.getClass().getName() +
	 * ": " + e.getMessage() ); System.exit(0); }
	 * 
	 * }
	 */
}
